from django.apps import AppConfig


class RiwayatTransaksiConfig(AppConfig):
    name = 'riwayat_transaksi'
