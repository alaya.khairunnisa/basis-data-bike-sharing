from django.shortcuts import render
from django.db import connection

def dafcer(request):
    response={}
    cursor = connection.cursor()
    cursor.execute('set search_path to dclf837m2cqhlg ,bike_sharing')
    cursor.execute("select v.id_voucher,v.nama,v.kategori,v.nilai_poin,v.deskripsi,per.nama from voucher v,person per,anggota a where v.no_kartu_anggota =a.no_kartu and a.ktp = per.ktp;")

    hasil1 = cursor.fetchall()
    response['voucher'] = hasil1
   

 
    return render(request, 'daftar_voucher.html',response)

def dafjem(request):
    response={}
    cursor = connection.cursor()
    cursor.execute('set search_path to dclf837m2cqhlg ,bike_sharing')
    cursor.execute("select pem.no_kartu_anggota, sep.jenis,st.nama,pem.datetime_kembali, pem.biaya,pem.denda from peminjaman pem,sepeda sep,stasiun st,anggota a where pem.no_kartu_anggota = a.no_kartu and pem.nomor_sepeda = sep.nomor and pem.id_stasiun = st.id_stasiun;")

    hasil1 = cursor.fetchall()
    response['peminjaman'] = hasil1
    
    return render(request, 'daftar_peminjaman.html',response)
	
def tambahvoucher(request):
    return render(request, 'create_voucher.html')

def updatevoucher(request):
    return render(request, 'update_voucher.html')

def pinjam(request):
    return render(request, 'create_peminjaman.html')

