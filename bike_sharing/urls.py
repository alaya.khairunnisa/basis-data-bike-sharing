"""bike_sharing URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url,include
from register import urls as register
from login import urls as login
from transaksi_topup import urls as transaksi_topup
from daftar_laporan import urls as daftar_laporan
from riwayat_transaksi import urls as riwayat_transaksi
from daftar_voucher import urls as daftar_voucher
from daftar_stasiun import urls as daftar_stasiun
from daftar_sepeda import urls as daftar_sepeda
from daftar_peminjaman import urls as daftar_peminjaman
from landing_page import urls as landing_page
from daftar_penugasan import urls as daftar_penugasan
from daftar_acara import urls as daftar_acara
from create_voucher import urls as create_voucher
from update_voucher import urls as update_voucher
from create_peminjaman import urls as create_peminjaman
from form_sepeda import urls as form_sepeda
from form_stasiun import urls as form_stasiun
from update_sepeda import urls as update_sepeda
from update_stasiun import urls as update_stasiun
from update_penugasan import urls as update_penugasan
from update_acara import urls as update_acara

urlpatterns = [
    url('admin/', admin.site.urls),
    url('', include(register)),
    url('',include(login)),
    url('',include(transaksi_topup)),
    url('',include(daftar_laporan)),
    url('',include(riwayat_transaksi)),
    url('',include(daftar_voucher)),
    url('',include(daftar_stasiun)),
    url('',include(daftar_sepeda)),
    url('',include(daftar_peminjaman)),
    url('',include(landing_page)),
    url('',include(daftar_penugasan)),
    url('',include(daftar_acara)),
    url('',include(create_voucher)),
    url('',include(update_voucher)),
    url('',include(create_peminjaman)),
    url('',include(update_stasiun)),
    url('',include(update_sepeda)),
    url('',include(form_stasiun)),
    url('',include(form_sepeda)),
    url('',include(update_penugasan)),
    url('',include(update_acara)),
]
