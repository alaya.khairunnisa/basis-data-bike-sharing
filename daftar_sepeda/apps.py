from django.apps import AppConfig


class DaftarSepedaConfig(AppConfig):
    name = 'daftar_sepeda'
