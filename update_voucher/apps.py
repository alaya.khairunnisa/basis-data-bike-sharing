from django.apps import AppConfig


class UpdateVoucherConfig(AppConfig):
    name = 'update_voucher'
