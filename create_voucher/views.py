from django.shortcuts import render

# Create your views here.
def create_voucher(request):
    return render(request, 'create_voucher.html')
