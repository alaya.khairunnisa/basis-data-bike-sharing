from django.apps import AppConfig


class CreateVoucherConfig(AppConfig):
    name = 'create_voucher'
