--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.22
-- Dumped by pg_dump version 10.8 (Ubuntu 10.8-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: bike_sharing; Type: SCHEMA; Schema: -; Owner: db2018002
--

CREATE SCHEMA bike_sharing;


ALTER SCHEMA bike_sharing OWNER TO db2018002;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: cek_peminjaman_lebih_satu_hari(); Type: FUNCTION; Schema: bike_sharing; Owner: db2018002
--

CREATE FUNCTION bike_sharing.cek_peminjaman_lebih_satu_hari() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
temp_row RECORD;
BEGIN
IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
FOR temp_row IN
SELECT no_kartu_anggota as a, datetime_pinjam as b, nomor_sepeda as c, id_stasiun as d
FROM PEMINJAMAN
GROUP BY no_kartu_anggota, datetime_pinjam, datetime_kembali
HAVING DATE_PART('day', datetime_kembali - datetime_pinjam) > 1
LOOP
INSERT INTO 
LAPORAN(id_laporan, no_kartu_anggota, datetime_pinjam, nomor_sepeda, id_stasiun, status)
VALUES (random(), temp_row.a, temp_row.b, temp_row.c, temp_row.d, random());
END LOOP;
END IF;
END;
$$;


ALTER FUNCTION bike_sharing.cek_peminjaman_lebih_satu_hari() OWNER TO db2018002;

--
-- Name: klaim_voucher(); Type: FUNCTION; Schema: bike_sharing; Owner: db2018002
--

CREATE FUNCTION bike_sharing.klaim_voucher() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
 BEGIN
  IF (TG_OP = 'UPDATE') THEN
   UPDATE ANGGOTA A
    SET points = points - NEW.nilai_poin
    WHERE A.no_kartu = NEW.no_kartu_anggota;
   RETURN NEW;
  END IF;
 END;
$$;


ALTER FUNCTION bike_sharing.klaim_voucher() OWNER TO db2018002;

--
-- Name: laporan_peminjaman_24jam(); Type: FUNCTION; Schema: bike_sharing; Owner: db2018002
--

CREATE FUNCTION bike_sharing.laporan_peminjaman_24jam() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
new_id CHARACTER VARYING(10);
selected_id CHARACTER VARYING(10);
BEGIN
IF(TG_OP='UPDATE') THEN
SELECT L.id_laporan FROM LAPORAN L
ORDER BY CAST(L.id_laporan as INTEGER)DESC
LIMIT 1 INTO selected_id;
SELECT CAST((CAST(selected_id as INTEGER)+1)
AS CHARACTER VARYING) INTO new_id;
IF(EXTRACT(EPOCH FROM(
NEW.datetime_kembali - OLD.datetime_pinjam))>86400)
THEN
INSERT INTO LAPORAN VALUES(
new_id,NEW.no_kartu_anggota,NEW.datetime_pinjam,
NEW.nomor_sepeda,NEW.id_stasiun,'Lebih dari 24 jam');
END IF;
RETURN NEW;
END IF;
END;
$$;


ALTER FUNCTION bike_sharing.laporan_peminjaman_24jam() OWNER TO db2018002;

--
-- Name: pengembalian_sepeda(); Type: FUNCTION; Schema: bike_sharing; Owner: db2018002
--

CREATE FUNCTION bike_sharing.pengembalian_sepeda() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    DECLARE 
        no_kartu_anggota_transaksi CHARACTER VARYING;
        date_time_pengembalian TIMESTAMP WITHOUT TIME ZONE;
    BEGIN
        IF (TG_OP = 'UPDATE') THEN
            SELECT T.no_kartu_anggota FROM TRANSAKSI T WHERE T.no_kartu_anggota = NEW.no_kartu_anggota AND T.date_time = NEW.datetime_pinjam INTO no_kartu_anggota_transaksi;
            SELECT T.date_time FROM TRANSAKSI T WHERE T.no_kartu_anggota = NEW.no_kartu_anggota AND T.date_time = NEW.datetime_pinjam INTO date_time_pengembalian;
            INSERT INTO TRANSAKSI_KHUSUS_PEMINJAMAN
                VALUES(
                    no_kartu_anggota_transaksi,
                    date_time_pengembalian,
                    NEW.no_kartu_anggota,
                    NEW.datetime_pinjam,
                    NEW.nomor_sepeda,
                    NEW.id_stasiun
                ); 
RETURN new;   
        END IF;   
    END;
$$;


ALTER FUNCTION bike_sharing.pengembalian_sepeda() OWNER TO db2018002;

--
-- Name: update_denda_10jam(); Type: FUNCTION; Schema: bike_sharing; Owner: db2018002
--

CREATE FUNCTION bike_sharing.update_denda_10jam() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF(TG_OP = 'UPDATE') THEN
IF(EXTRACT(EPOCH FROM(NEW.datetime_kembali - OLD.datetime_pinjam))>86400)
THEN
UPDATE PEMINJAMAN P
SET denda = denda + 3000000
WHERE NEW.no_kartu_anggota = P.no_kartu_anggota
AND NEW.datetime_pinjam = P.datetime_pinjam
AND NEW.nomor_sepeda = P.nomor_sepeda
AND NEW.id_stasiun = P.id_stasiun;
ELSIF(EXTRACT(EPOCH FROM(NEW.datetime_kembali - OLD.datetime_pinjam))>36000)
THEN
UPDATE PEMINJAMAN P
SET denda = denda +(5000*((EXTRACT(
EPOCH FROM(datetime_kembali - datetime_pinjam))/3600)-10))
WHERE NEW.no_kartu_anggota = P.no_kartu_anggota
AND NEW.datetime_pinjam = P.datetime_pinjam
AND NEW.nomor_sepeda = P.nomor_sepeda
AND NEW.id_stasiun = P.id_stasiun;
END IF;
RETURN NEW;
END IF;
END;
$$;


ALTER FUNCTION bike_sharing.update_denda_10jam() OWNER TO db2018002;

--
-- Name: cek_peminjaman_lebih_satu_hari(); Type: FUNCTION; Schema: public; Owner: db2018002
--

CREATE FUNCTION public.cek_peminjaman_lebih_satu_hari() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
temp_row RECORD;
BEGIN
IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
FOR temp_row IN
SELECT no_kartu_anggota as a, datetime_pinjam as b, nomor_sepeda as c, id_stasiun as d
FROM PEMINJAMAN
GROUP BY no_kartu_anggota, datetime_pinjam, datetime_kembali
HAVING DATE_PART('day', datetime_kembali - datetime_pinjam) > 1
LOOP
INSERT INTO 
LAPORAN(id_laporan, no_kartu_anggota, datetime_pinjam, nomor_sepeda, id_stasiun, status)
VALUES (random(), temp_row.a, temp_row.b, temp_row.c, temp_row.d, random());
END LOOP;
END IF;
END;
$$;


ALTER FUNCTION public.cek_peminjaman_lebih_satu_hari() OWNER TO db2018002;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: acara; Type: TABLE; Schema: bike_sharing; Owner: db2018002
--

CREATE TABLE bike_sharing.acara (
    id_acara character varying(10) NOT NULL,
    judul character varying(100) NOT NULL,
    deskripsi text,
    tgl_mulai date NOT NULL,
    tgl_akhir date NOT NULL,
    is_free boolean NOT NULL
);


ALTER TABLE bike_sharing.acara OWNER TO db2018002;

--
-- Name: acara_stasiun; Type: TABLE; Schema: bike_sharing; Owner: db2018002
--

CREATE TABLE bike_sharing.acara_stasiun (
    id_stasiun character varying(10) NOT NULL,
    id_acara character varying(10) NOT NULL
);


ALTER TABLE bike_sharing.acara_stasiun OWNER TO db2018002;

--
-- Name: anggota; Type: TABLE; Schema: bike_sharing; Owner: db2018002
--

CREATE TABLE bike_sharing.anggota (
    no_kartu character varying(10) NOT NULL,
    saldo real,
    points integer,
    ktp character varying(20) NOT NULL
);


ALTER TABLE bike_sharing.anggota OWNER TO db2018002;

--
-- Name: laporan; Type: TABLE; Schema: bike_sharing; Owner: db2018002
--

CREATE TABLE bike_sharing.laporan (
    id_laporan character varying(10) NOT NULL,
    no_kartu_anggota character varying(10) NOT NULL,
    datetime_pinjam timestamp without time zone NOT NULL,
    nomor_sepeda character varying(10) NOT NULL,
    id_stasiun character varying(10) NOT NULL,
    status character varying(20) NOT NULL
);


ALTER TABLE bike_sharing.laporan OWNER TO db2018002;

--
-- Name: peminjaman; Type: TABLE; Schema: bike_sharing; Owner: db2018002
--

CREATE TABLE bike_sharing.peminjaman (
    no_kartu_anggota character varying(10) NOT NULL,
    datetime_pinjam timestamp without time zone NOT NULL,
    nomor_sepeda character varying(10) NOT NULL,
    id_stasiun character varying(10) NOT NULL,
    datetime_kembali timestamp without time zone,
    biaya real,
    denda real
);


ALTER TABLE bike_sharing.peminjaman OWNER TO db2018002;

--
-- Name: penugasan; Type: TABLE; Schema: bike_sharing; Owner: db2018002
--

CREATE TABLE bike_sharing.penugasan (
    ktp character varying(20) NOT NULL,
    start_datetime timestamp without time zone NOT NULL,
    id_stasiun character varying(10) NOT NULL,
    end_datetime timestamp without time zone NOT NULL
);


ALTER TABLE bike_sharing.penugasan OWNER TO db2018002;

--
-- Name: person; Type: TABLE; Schema: bike_sharing; Owner: db2018002
--

CREATE TABLE bike_sharing.person (
    ktp character varying(20) NOT NULL,
    email character varying(50) NOT NULL,
    nama character varying(50) NOT NULL,
    alamat text,
    tgl_lahir date NOT NULL,
    no_telp character varying(20)
);


ALTER TABLE bike_sharing.person OWNER TO db2018002;

--
-- Name: petugas; Type: TABLE; Schema: bike_sharing; Owner: db2018002
--

CREATE TABLE bike_sharing.petugas (
    ktp character varying(20) NOT NULL,
    gaji real NOT NULL
);


ALTER TABLE bike_sharing.petugas OWNER TO db2018002;

--
-- Name: sepeda; Type: TABLE; Schema: bike_sharing; Owner: db2018002
--

CREATE TABLE bike_sharing.sepeda (
    nomor character varying(10) NOT NULL,
    merk character varying(10) NOT NULL,
    jenis character varying(50) NOT NULL,
    status boolean NOT NULL,
    id_stasiun character varying(10) NOT NULL,
    no_kartu_penyumbang character varying(20)
);


ALTER TABLE bike_sharing.sepeda OWNER TO db2018002;

--
-- Name: stasiun; Type: TABLE; Schema: bike_sharing; Owner: db2018002
--

CREATE TABLE bike_sharing.stasiun (
    id_stasiun character varying(10) NOT NULL,
    alamat text NOT NULL,
    lat real,
    long real,
    nama character varying(50) NOT NULL
);


ALTER TABLE bike_sharing.stasiun OWNER TO db2018002;

--
-- Name: transaksi; Type: TABLE; Schema: bike_sharing; Owner: db2018002
--

CREATE TABLE bike_sharing.transaksi (
    no_kartu_anggota character varying(10) NOT NULL,
    date_time timestamp without time zone NOT NULL,
    jenis character varying(20) NOT NULL,
    nominal real NOT NULL
);


ALTER TABLE bike_sharing.transaksi OWNER TO db2018002;

--
-- Name: transaksi_khusus_peminjaman; Type: TABLE; Schema: bike_sharing; Owner: db2018002
--

CREATE TABLE bike_sharing.transaksi_khusus_peminjaman (
    no_kartu_anggota character varying(10) NOT NULL,
    date_time timestamp without time zone NOT NULL,
    datetime_pinjam timestamp without time zone NOT NULL,
    no_sepeda character varying(10) NOT NULL,
    id_stasiun character varying(10) NOT NULL,
    no_kartu_peminjam character varying(10) NOT NULL
);


ALTER TABLE bike_sharing.transaksi_khusus_peminjaman OWNER TO db2018002;

--
-- Name: voucher; Type: TABLE; Schema: bike_sharing; Owner: db2018002
--

CREATE TABLE bike_sharing.voucher (
    id_voucher character varying(10) NOT NULL,
    nama character varying(255) NOT NULL,
    kategori character varying(255) NOT NULL,
    nilai_poin real NOT NULL,
    deskripsi text,
    no_kartu_anggota character varying(10) NOT NULL
);


ALTER TABLE bike_sharing.voucher OWNER TO db2018002;

--
-- Data for Name: acara; Type: TABLE DATA; Schema: bike_sharing; Owner: db2018002
--

COPY bike_sharing.acara (id_acara, judul, deskripsi, tgl_mulai, tgl_akhir, is_free) FROM stdin;
31767415sy	Murder Mystery Night	Get your team working together to hunt down that murderer.	2019-04-14	2019-04-23	t
21328387ve	Ping Pong Day	Guarantee someone on your team will be secretly awesome at ping pong.	2019-04-14	2019-04-22	t
28989208ha	The Annual Fundraising event	Why not get members involved with hosting a charity picnic, fayre or ball?	2019-04-13	2019-04-24	t
28051072nj	Be a fundraiser for the day	You and your team could volunteer to take to the streets and fundraise for a charity	2019-04-12	2019-04-19	f
27892701oz	The Community voluntary event	There are thousands of charities that would offer you the chance to get out into the community	2019-04-14	2019-04-23	t
62847283yk	Clean up projects	 Probably the least glamorous, but someone needs to do it.	2019-04-18	2019-04-25	t
78198365fr	The Highland Games	With classics like Scottish Hammer Throw and Caber Toss. What is not to love?	2019-04-15	2019-04-23	f
19047404xd	Pokmon Hunt on Bike	Pick them out of a hat else you will probably end up with masses of Pikachus.	2019-04-16	2019-04-23	f
60231995mj	Spa weekend	Treat your members by take them out to a lovely, relaxing spa!	2019-12-04	2019-04-20	t
04011983ds	Mad Hatters tea party	A twist on the good, old-fashioned afternoon tea.	2019-04-16	2019-04-22	f
\.


--
-- Data for Name: acara_stasiun; Type: TABLE DATA; Schema: bike_sharing; Owner: db2018002
--

COPY bike_sharing.acara_stasiun (id_stasiun, id_acara) FROM stdin;
82603zeunj	31767415sy
57875ybgyd	21328387ve
35123bcoml	28989208ha
95847vroeq	28051072nj
14283rjsbe	27892701oz
83631zntvy	62847283yk
07717dktkx	78198365fr
74776kgrfu	19047404xd
78444cyymk	60231995mj
02234frlyc	04011983ds
72979dgmko	31767415sy
80518xpqcc	21328387ve
76166fzebv	28989208ha
96802mffoa	28051072nj
71095etfav	27892701oz
11167kfqsv	62847283yk
10260xtlct	78198365fr
55763zfkpb	19047404xd
38835ibdzg	60231995mj
23284fvmdm	04011983ds
11167kfqsv	27892701oz
10260xtlct	62847283yk
55763zfkpb	78198365fr
38835ibdzg	21328387ve
23284fvmdm	28989208ha
\.


--
-- Data for Name: anggota; Type: TABLE DATA; Schema: bike_sharing; Owner: db2018002
--

COPY bike_sharing.anggota (no_kartu, saldo, points, ktp) FROM stdin;
9149189752	464	16	814192644
3998219323	53	16	664069096
5148092163	155	30	653945889
5214768741	469	17	706732316
5996959807	315	10	554417014
3075377380	261	19	514310612
4469252291	119	12	698206999
4626148636	85	18	588067393
6645766128	387	21	861330382
2890432873	64	23	577750334
2064546969	143	17	471681755
8025852779	137	16	564270036
4647444213	41	8	438433380
8918435344	175	26	409494820
5105047188	497	20	711857733
6742917276	448	25	403272864
2935403024	121	29	797652945
8368924743	26	2	588810091
3219251815	203	7	715088126
7268249915	136	26	453275872
7687053121	54	21	604821144
8777843452	199	28	461970034
8030644827	305	23	409528293
1691875253	114	16	626888193
3240423501	464	10	609742754
3798361905	386	2	791520565
3306649912	475	6	635443524
8693543197	170	5	526438853
9657487871	175	8	594217709
6886200720	409	6	460633671
2478836858	340	9	481572448
5181172361	458	10	632660705
7017915128	120	30	874270108
1106065915	204	26	464518602
1962223574	291	24	534544518
1424222828	256	5	496369656
8281116731	269	5	594667576
3279081878	37	4	800790126
5272592791	368	22	698480328
5610582560	48	19	790267586
3750241908	138	6	812978175
3623014849	109	23	633365764
7761353247	494	5	515540697
9166517431	82	12	455623234
7182858885	238	30	651009370
4347265556	77	1	801907031
1466981905	100	20	640310873
4371123076	203	22	698788106
4253457701	470	26	449406697
8668430755	109	5	405897876
1910748638	141	10	652830824
2389575077	153	18	530625039
6195620957	180	12	524570242
7129431658	34	4	479289867
3672180261	179	16	433142063
8719052629	377	7	817273106
8959826142	359	8	417125379
6991968211	246	4	437540452
5105856747	362	16	433055700
9435880197	457	30	584044794
7564879146	103	10	805297341
9522484954	270	7	435068063
1534736738	309	25	425785893
6109449585	54	6	899601279
1459534240	159	13	563481946
5314254023	370	3	756839251
7605910199	461	27	570280012
2270200232	94	17	588653831
2798530287	89	12	494584163
9363730799	141	25	823201631
1187226485	328	67	794487851
5516271674	206	91	658084567
7361041422	993	20	486439747
5859087413	581	93	465459684
9681539083	363	47	748066729
2557960076	129	68	439973497
8379265511	150	86	597854125
7971565005	125	81	617930961
9975399997	695	6	629563891
2963452256	906	90	856838873
3640102210	711	97	418776198
3495158744	807	3	559154664
4562780828	150	20	809432865
5064108343	559	31	874204499
1711407956	450	31	457107084
2247662037	839	83	594273787
9681571865	928	69	592538424
2060971946	702	69	401585352
6226946821	517	71	535014262
2587202383	130	36	773494757
1128961932	161	69	703608997
8047287471	836	67	828423834
3775569364	958	48	405149053
8928320972	33	77	805246274
3221489332	529	49	726962856
1932696029	574	71	566825231
5042113319	373	6	539280861
5687039417	474	80	829336344
2445831414	558	49	571756607
1197062072	698	83	611092978
\.


--
-- Data for Name: laporan; Type: TABLE DATA; Schema: bike_sharing; Owner: db2018002
--

COPY bike_sharing.laporan (id_laporan, no_kartu_anggota, datetime_pinjam, nomor_sepeda, id_stasiun, status) FROM stdin;
12345678aa	9149189752	2019-05-03 22:44:47	5888268302	82603zeunj	BARU MASUK
23456789ab	3998219323	2019-04-25 00:09:37	7707568500	57875ybgyd	BARU MASUK
2355468ebc	5148092163	2019-04-25 22:31:39	5030450187	35123bcoml	BARU MASUK
67384676bb	5214768741	2019-04-25 18:32:16	5913641843	95847vroeq	BARU MASUK
78364173ab	5996959807	2019-04-09 22:26:01	6175065676	14283rjsbe	SUDAH DITANGANI
28936177ac	3075377380	2019-04-14 14:33:39	5491339677	83631zntvy	SUDAH DITANGANI
21456735ba	4469252291	2019-04-22 12:35:41	5883229899	07717dktkx	SUDAH DITANGANI
97642876bc	4626148636	2019-04-28 11:15:48	6830851012	74776kgrfu	SUDAH DITANGANI
17632853cc	9363730799	2019-04-24 06:35:56	6148386169	78444cyymk	SUDAH SELESAI
43562783cc	6645766128	2019-05-01 13:27:30	4700719454	02234frlyc	SUDAH SELESAI
\.


--
-- Data for Name: peminjaman; Type: TABLE DATA; Schema: bike_sharing; Owner: db2018002
--

COPY bike_sharing.peminjaman (no_kartu_anggota, datetime_pinjam, nomor_sepeda, id_stasiun, datetime_kembali, biaya, denda) FROM stdin;
9149189752	2019-05-03 22:44:47	5888268302	82603zeunj	2019-05-29 22:29:12	13	13
3998219323	2019-04-25 00:09:37	7707568500	57875ybgyd	2019-06-07 16:46:30	14	9
5148092163	2019-04-25 22:31:39	5030450187	35123bcoml	2019-05-14 06:20:13	11	8
5214768741	2019-04-25 18:32:16	5913641843	95847vroeq	2019-06-02 12:28:52	12	2
5996959807	2019-04-09 22:26:01	6175065676	14283rjsbe	2019-05-11 05:12:55	11	15
3075377380	2019-04-14 14:33:39	5491339677	83631zntvy	2019-05-29 02:54:04	13	28
4469252291	2019-04-22 12:35:41	5883229899	07717dktkx	2019-05-21 19:41:30	10	14
4626148636	2019-04-28 11:15:48	6830851012	74776kgrfu	2019-05-23 05:28:00	14	10
9363730799	2019-04-24 06:35:56	6148386169	78444cyymk	2019-05-17 17:28:18	12	7
6645766128	2019-05-01 13:27:30	4700719454	02234frlyc	2019-06-01 15:35:23	14	18
2890432873	2019-05-05 11:47:10	1914491831	72979dgmko	2019-06-08 17:30:11	14	30
2064546969	2019-04-16 21:22:30	6890797479	80518xpqcc	2019-06-08 08:30:57	11	25
8025852779	2019-04-13 12:07:20	7020393352	76166fzebv	2019-05-26 22:48:41	10	14
4647444213	2019-04-11 06:55:45	5239918453	96802mffoa	2019-05-21 04:46:56	10	3
8918435344	2019-05-06 19:54:55	5592820241	71095etfav	2019-05-23 06:04:44	11	11
5105047188	2019-05-02 09:41:19	1376300062	11167kfqsv	2019-05-21 21:20:21	10	28
6742917276	2019-04-11 11:22:09	6759458308	10260xtlct	2019-05-27 05:52:54	10	4
2935403024	2019-04-28 02:06:38	7210486846	55763zfkpb	2019-06-04 16:48:04	12	23
8368924743	2019-04-13 09:36:05	7675313474	38835ibdzg	2019-05-24 13:31:49	10	24
3219251815	2019-04-23 08:34:06	6849050033	23284fvmdm	2019-05-18 11:18:07	13	27
7268249915	2019-05-07 13:40:51	9151849413	98303tmtil	2019-06-04 14:23:38	12	25
7687053121	2019-05-08 22:16:18	2764292488	97887cpuuf	2019-05-15 09:55:02	14	17
8777843452	2019-04-26 14:11:56	6052896889	34074qkdqk	2019-05-20 05:53:30	14	24
8030644827	2019-05-07 12:40:10	7799584746	69374txmjx	2019-05-30 17:38:10	15	7
1691875253	2019-05-02 22:42:14	1278212286	13995olzwn	2019-05-11 21:15:29	13	25
3240423501	2019-04-09 22:06:27	3671879154	81953mmaji	2019-05-19 05:38:03	11	25
3798361905	2019-04-21 12:30:37	6507178510	05876tvgpx	2019-05-19 08:46:39	14	25
3306649912	2019-04-29 03:54:16	6073009026	34856egpyx	2019-06-08 05:58:12	10	24
8693543197	2019-04-19 02:31:12	6977220455	39792xcnid	2019-05-31 16:58:37	14	12
9657487871	2019-04-19 15:55:22	3373518528	09761mfyny	2019-05-14 10:13:43	10	10
6886200720	2019-05-05 14:44:06	9274471006	92870xdrju	2019-05-18 00:51:25	10	14
2478836858	2019-04-15 01:33:51	4325557029	82182rkhkn	2019-05-11 22:33:47	13	17
5181172361	2019-05-08 20:07:11	2998245982	95767qrzah	2019-05-31 10:21:43	14	29
7017915128	2019-04-25 20:16:36	3157493341	59817qgxhx	2019-05-23 09:31:13	13	10
1106065915	2019-05-07 04:24:18	1924667002	93812jwroq	2019-05-24 20:55:19	14	15
1962223574	2019-04-15 02:00:38	4955449246	03773emjre	2019-05-13 02:35:22	13	24
1424222828	2019-04-12 03:24:14	8143518282	44977ncosu	2019-05-20 00:02:24	13	21
8281116731	2019-04-16 00:32:22	4706723123	83095uslnh	2019-06-01 05:00:47	12	4
3279081878	2019-04-24 08:54:50	8588954889	62030ymtoa	2019-05-27 05:19:05	11	15
5272592791	2019-05-07 18:56:34	8100609914	45164kiyjh	2019-05-23 16:35:25	10	18
5610582560	2019-04-17 20:29:17	9150727205	74851wnism	2019-06-09 06:52:53	10	27
3750241908	2019-04-12 22:03:26	9168412789	40451bfxpc	2019-05-19 12:34:58	14	14
3623014849	2019-04-12 04:07:45	6500215158	81307jltbl	2019-05-15 16:47:51	11	1
7761353247	2019-05-03 07:16:31	3927503961	79919hzkqq	2019-05-12 10:57:31	13	24
9166517431	2019-04-21 02:05:49	4608545041	95071oyzes	2019-06-01 16:49:13	15	24
7182858885	2019-04-22 00:43:34	9999482046	94375cephj	2019-05-15 20:45:23	11	18
4347265556	2019-04-15 22:28:14	4305358562	62290vuypd	2019-05-28 19:14:32	14	7
1466981905	2019-04-11 14:28:38	1859224963	91564cgsvo	2019-05-10 08:49:38	11	3
4371123076	2019-04-09 11:02:11	6502094853	81229qqwea	2019-05-24 17:14:22	13	27
4253457701	2019-04-19 20:18:10	7962884733	20501dnyrk	2019-05-10 05:40:55	14	2
\.


--
-- Data for Name: penugasan; Type: TABLE DATA; Schema: bike_sharing; Owner: db2018002
--

COPY bike_sharing.penugasan (ktp, start_datetime, id_stasiun, end_datetime) FROM stdin;
814192644	2019-11-23 05:00:00	82603zeunj	2019-12-23 23:00:00
664069096	2019-11-23 05:00:00	57875ybgyd	2019-12-23 23:00:00
653945889	2019-11-23 05:00:00	35123bcoml	2019-12-23 23:00:00
706732316	2019-11-23 05:00:00	95847vroeq	2019-12-23 23:00:00
554417014	2019-11-24 05:00:00	14283rjsbe	2019-12-24 23:00:00
514310612	2019-11-24 05:00:00	83631zntvy	2019-12-24 23:00:00
698206999	2019-11-25 05:00:00	07717dktkx	2019-12-25 23:00:00
588067393	2019-11-25 05:00:00	74776kgrfu	2019-12-25 23:00:00
823201631	2019-11-25 05:00:00	78444cyymk	2019-12-25 23:00:00
861330382	2019-11-26 05:00:00	02234frlyc	2019-12-26 23:00:00
577750334	2019-11-26 05:00:00	72979dgmko	2019-12-26 23:00:00
471681755	2019-11-26 05:00:00	80518xpqcc	2019-12-26 23:00:00
564270036	2019-11-26 05:00:00	76166fzebv	2019-12-26 23:00:00
438433380	2019-11-27 05:00:00	96802mffoa	2019-12-27 23:00:00
409494820	2019-11-27 05:00:00	71095etfav	2019-12-27 23:00:00
711857733	2019-11-28 05:00:00	11167kfqsv	2019-12-28 23:00:00
403272864	2019-11-29 05:00:00	10260xtlct	2019-12-29 23:00:00
797652945	2019-11-29 05:00:00	55763zfkpb	2019-12-29 23:00:00
588810091	2019-11-29 05:00:00	38835ibdzg	2019-12-29 23:00:00
715088126	2019-11-30 05:00:00	23284fvmdm	2019-12-30 23:00:00
453275872	2019-11-30 05:00:00	82603zeunj	2019-12-30 23:00:00
604821144	2019-11-30 05:00:00	57875ybgyd	2019-12-30 23:00:00
461970034	2019-11-30 05:00:00	35123bcoml	2019-12-30 23:00:00
409528293	2019-12-01 05:00:00	95847vroeq	2019-12-31 23:00:00
626888193	2019-12-01 05:00:00	14283rjsbe	2019-12-31 23:00:00
609742754	2019-12-02 05:00:00	83631zntvy	2017-01-01 23:00:00
791520565	2019-12-02 05:00:00	07717dktkx	2017-01-01 23:00:00
635443524	2019-12-02 05:00:00	74776kgrfu	2017-01-01 23:00:00
526438853	2019-12-03 05:00:00	78444cyymk	2017-01-02 23:00:00
594217709	2019-12-03 05:00:00	02234frlyc	2017-01-02 23:00:00
\.


--
-- Data for Name: person; Type: TABLE DATA; Schema: bike_sharing; Owner: db2018002
--

COPY bike_sharing.person (ktp, email, nama, alamat, tgl_lahir, no_telp) FROM stdin;
814192644	cpepperd0@cbslocal.com	Cyrillus Pepperd	48 Bowman Alley	1999-08-16	7077776215
664069096	mmyton1@hexun.com	Melanie Myton	0 Morrow Junction	1992-04-20	9434862894
653945889	ezettler2@sphinn.com	Esme Zettler	10 Stephen Park	2001-01-03	2312436244
706732316	hhumphris3@i2i.jp	Hilde Humphris	15 Fisk Alley	1993-04-20	9196483872
554417014	jmaton4@vk.com	Janeen Maton	2 Buell Crossing	1999-09-11	2786117511
514310612	mhail5@issuu.com	Max Hail	2337 Messerschmidt Place	1995-07-21	9448205341
698206999	gslocum6@usgs.gov	Gates Slocum	431 Golden Leaf Pass	1991-05-11	2388916375
588067393	mgwinnett7@slideshare.net	Maureene Gwinnett	88 Maple Pass	2000-03-07	5575466702
823201631	rroyson8@weibo.com	Rachelle Royson	8943 Homewood Point	2000-10-16	4853750383
861330382	tstreather9@behance.net	Tiffi Streather	855 Pond Junction	1993-07-16	6511096561
577750334	rschruursa@msn.com	Rozanne Schruurs	95695 Kennedy Park	1996-10-23	3682964540
471681755	kveelerb@indiatimes.com	Karalee Veeler	0817 Valley Edge Road	2000-02-06	7028114390
564270036	dmcc@imgur.com	Desmond Mc Harg	0866 Roxbury Avenue	1996-06-29	8362765142
438433380	olarimerd@harvard.edu	Ozzy Larimer	7 Erie Parkway	1993-08-31	5563606137
409494820	ngriptone@woothemes.com	Nathan Gripton	6 Loomis Junction	1990-06-17	7651877355
711857733	shardinf@ameblo.jp	Sherilyn Hardin	94161 Hintze Avenue	1996-09-30	7465744660
403272864	kfillong@mac.com	Kyrstin Fillon	127 Orin Junction	1995-06-19	3707150544
797652945	zwavishh@statcounter.com	Zebadiah Wavish	94227 Manley Point	1991-11-09	9935466333
588810091	ddelgardoi@aol.com	Dunc Delgardo	20 Roxbury Street	1999-10-19	7225130253
715088126	hbaressj@imgur.com	Hertha Baress	5968 Vermont Trail	1992-06-12	6435244373
453275872	cslefordk@themeforest.net	Charita Sleford	341 Basil Way	1990-08-22	6895388261
604821144	chartlandl@sphinn.com	Carney Hartland	1707 Merchant Park	1992-11-07	8992313604
461970034	hclothierm@hostgator.com	Herminia Clothier	3 Merry Circle	2000-05-06	6767936177
409528293	athomazinn@webs.com	Angelico Thomazin	8950 Calypso Parkway	1997-07-28	1718183356
626888193	vbennero@zdnet.com	Valle Benner	4 Sachtjen Plaza	1998-07-02	8325831410
609742754	dlempennyp@topsy.com	Derrik Lempenny	4 Merchant Road	1996-08-23	3183571509
791520565	fbullasq@prweb.com	Forrest Bullas	61 Trailsway Place	1993-06-26	9391153005
635443524	aburtonwoodr@goodreads.com	Afton Burtonwood	390 Del Sol Drive	1996-01-15	4515649065
526438853	gstockwells@github.io	Gianina Stockwell	604 Hanson Center	1997-07-10	9741250167
594217709	ilinekert@weibo.com	Issy Lineker	49344 Ramsey Center	1990-05-09	7123293367
460633671	rscohieru@addthis.com	Rasla Scohier	0707 Gulseth Pass	1999-08-03	9732617884
481572448	vmccleanv@godaddy.com	Victoria McClean	8 Tony Terrace	2000-03-02	8035132133
632660705	cundrellw@blinklist.com	Cart Undrell	0 Sundown Hill	1995-04-24	8746469858
874270108	slex@woothemes.com	Sigismundo Le Quesne	7379 Corscot Center	1992-10-09	6721216039
464518602	lloadesy@businessinsider.com	Lizzy Loades	80954 Killdeer Way	1994-09-01	2687766835
534544518	aavrahmz@blogs.com	Anet Avrahm	13 Bayside Court	1990-08-03	8721044027
496369656	swabb10@seattletimes.com	Stinky Wabb	83175 Veith Circle	1992-12-01	7201921800
594667576	navard11@istockphoto.com	Noreen Avard	0624 5th Drive	1994-02-24	9734995484
800790126	esaing12@fda.gov	Elset Saing	573 Gina Drive	1997-05-25	9435993647
698480328	mgulk13@list-manage.com	Merilyn Gulk	8630 Ridge Oak Hill	1999-06-13	9912739454
790267586	llowsely14@arstechnica.com	Linoel Lowsely	27468 Sutherland Road	1990-11-02	2811963407
812978175	ebilam15@redcross.org	Eleanore Bilam	59 West Center	2000-12-11	5148619841
633365764	eeaton16@infoseek.co.jp	Errol Eaton	7396 Lakewood Road	1991-03-24	5889104732
515540697	ajaycocks17@linkedin.com	Ancell Jaycocks	802 Troy Lane	1991-02-17	3513188825
455623234	cgeorgi18@alibaba.com	Claire Georgi	4 Mifflin Circle	1995-02-15	1157998760
651009370	ldann19@hao123.com	Louie Dann	6505 Jay Circle	2000-01-04	4424328273
801907031	lpacitti1a@360.cn	Lenora Pacitti	44953 Golf Avenue	1997-03-09	5876985501
640310873	mvoas1b@slideshare.net	Morgen Voas	56112 Grasskamp Crossing	1991-11-06	9147139322
698788106	rpeakman1c@fda.gov	Rosita Peakman	7 Aberg Junction	1996-10-04	6203312492
449406697	redensor1d@netscape.com	Rance Edensor	9 Havey Park	1994-10-06	5199839738
405897876	hrennocks1e@chronoengine.com	Harlen Rennocks	072 Dexter Drive	1994-07-10	2473142902
652830824	xfarron1f@blogspot.com	Xenos Farron	62575 Anderson Street	1992-07-08	9131097929
530625039	aledgerton1g@huffingtonpost.com	Ari Ledgerton	59 Stoughton Road	1995-08-23	5213816443
524570242	bluttger1h@list-manage.com	Booth Luttger	21 Northridge Terrace	2000-01-06	1871286478
479289867	tgockeler1i@cdc.gov	Teriann Gockeler	1552 Welch Junction	1994-08-20	3524295838
433142063	etettersell1j@bizjournals.com	Etienne Tettersell	830 Westerfield Way	1991-11-03	6102818821
817273106	rmozzetti1k@auda.org.au	Rica Mozzetti	9945 Namekagon Trail	1998-08-09	3732188704
417125379	ddeaville1l@oaic.gov.au	Doralia Deaville	0896 Fair Oaks Avenue	1994-12-26	4369187308
437540452	oreeders1m@networkadvertising.org	Obed Reeders	50414 Stone Corner Parkway	1994-11-29	8619123678
433055700	bmaffulli1n@guardian.co.uk	Brandea Maffulli	58062 David Court	1997-10-24	7759929390
584044794	bleiden1o@tiny.cc	Bryana Leiden	4996 Pearson Way	1991-11-23	7267847684
805297341	dwhiston1p@salon.com	Dolf Whiston	164 Mcguire Crossing	1994-09-02	1433720205
435068063	gbyers1q@addtoany.com	Germana Byers	7887 Sheridan Park	2000-08-12	5369232754
425785893	lmaybery1r@weebly.com	Lily Maybery	24978 American Ash Point	1999-03-24	7971364727
899601279	edionisetti1s@slashdot.org	Evin Dionisetti	59 Coleman Junction	1994-04-27	2178325198
563481946	lbarwell1t@1688.com	Lorry Barwell	2232 Hayes Drive	1999-06-06	6054304025
756839251	eespinho1u@blogtalkradio.com	Elli Espinho	78663 Summer Ridge Alley	1998-12-17	4222736585
570280012	djursch1v@buzzfeed.com	Dulsea Jursch	625 Mosinee Court	1992-12-15	3193710857
588653831	rshellard1w@cafepress.com	Reeta Shellard	14324 Butterfield Center	1994-12-17	3189195468
494584163	hplover1x@typepad.com	Hyacinthia Plover	888 Continental Court	1994-11-17	5818527226
794487851	fissard1y@hhs.gov	Fletcher Issard	315 Amoth Avenue	1992-10-18	6935395116
658084567	rwoodwind1z@about.me	Rora Woodwind	0 Vidon Point	1996-06-07	8786476342
486439747	ewiszniewski20@timesonline.co.uk	Erik Wiszniewski	790 Oxford Terrace	2000-09-10	7845332091
465459684	jbergen21@ycombinator.com	Jane Bergen	009 Cardinal Avenue	1995-07-30	4854536009
748066729	sriepl22@hibu.com	Scarface Riepl	13222 Rieder Avenue	1993-04-05	9997425832
439973497	nlesser23@over-blog.com	Nate Lesser	685 Tennyson Street	1993-10-02	8582488024
597854125	dmival24@goo.ne.jp	Doe Mival	39 Texas Point	1992-04-24	4485489593
617930961	lfonquernie25@addthis.com	Lilly Fonquernie	2341 Forest Run Plaza	1996-06-18	4014251542
629563891	gmoffat26@vk.com	Gabbie Moffat	12202 Holmberg Terrace	1994-08-07	2072751354
856838873	rhammand27@nih.gov	Ranna Hammand	25541 Fisk Point	2000-10-25	4072018143
418776198	rlight28@topsy.com	Reinaldos Light	2 Ridgeview Terrace	1998-02-03	3298142206
559154664	sgillespie29@trellian.com	Schuyler Gillespie	66 Di Loreto Point	1991-01-25	9951325903
809432865	mtriggs2a@w3.org	Marlane Triggs	80 Wayridge Circle	1990-04-09	1719397210
874204499	snodin2b@etsy.com	Shela Nodin	726 Mockingbird Center	1990-10-31	5706584035
457107084	amacdougal2c@tiny.cc	Audi MacDougal	77 Clarendon Place	2000-08-20	5313113722
594273787	caery2d@godaddy.com	Conrad Aery	5 Sloan Lane	1995-05-06	8963819157
592538424	rgebuhr2e@pen.io	Rivy Gebuhr	819 Hoepker Road	1993-05-28	8375995313
401585352	syarranton2f@sfgate.com	Sheila-kathryn Yarranton	793 American Ash Road	1994-06-12	7344889111
535014262	gwolpert2g@google.nl	Gwennie Wolpert	7225 John Wall Point	1997-12-10	8266382465
773494757	kmcconachie2h@digg.com	Keane McConachie	940 Judy Terrace	1999-02-19	5982313970
703608997	pkellie2i@delicious.com	Pace Kellie	95035 Iowa Circle	1993-01-08	6162318557
828423834	kabbatt2j@oracle.com	Kip Abbatt	7150 Northview Park	2001-02-02	3438365092
405149053	gbegent2k@hud.gov	Garnet Begent	23622 Browning Street	1992-07-05	9823062760
805246274	epaulig2l@github.io	Emmaline Paulig	2128 Onsgard Trail	1991-05-20	7784055357
726962856	hmerricks2m@mapquest.com	Hyacinth Merricks	84 Pleasure Avenue	1999-05-31	9648416165
566825231	mwhitfield2n@howstuffworks.com	Martha Whitfield	8727 Killdeer Parkway	1991-09-12	6301630033
539280861	ptraher2o@quantcast.com	Petey Traher	8 Armistice Trail	2000-09-02	2568921801
829336344	caxtonne2p@walmart.com	Chrissy Axtonne	08524 Center Crossing	1994-04-14	9009984209
571756607	rgeaves2q@newyorker.com	Rufe Geaves	180 Gateway Terrace	1990-09-14	2053958773
611092978	hrenforth2r@jiathis.com	Hugibert Renforth	3483 Bay Avenue	1997-06-24	5874356028
\.


--
-- Data for Name: petugas; Type: TABLE DATA; Schema: bike_sharing; Owner: db2018002
--

COPY bike_sharing.petugas (ktp, gaji) FROM stdin;
814192644	10000000
664069096	15000000
653945889	20000000
706732316	30000000
554417014	40000000
514310612	10000000
698206999	15000000
588067393	20000000
823201631	30000000
861330382	40000000
577750334	10000000
471681755	15000000
564270036	20000000
438433380	30000000
409494820	40000000
711857733	10000000
403272864	15000000
797652945	20000000
588810091	30000000
715088126	40000000
453275872	10000000
604821144	15000000
461970034	20000000
409528293	30000000
626888193	40000000
609742754	10000000
791520565	15000000
635443524	20000000
526438853	30000000
594217709	40000000
\.


--
-- Data for Name: sepeda; Type: TABLE DATA; Schema: bike_sharing; Owner: db2018002
--

COPY bike_sharing.sepeda (nomor, merk, jenis, status, id_stasiun, no_kartu_penyumbang) FROM stdin;
5888268302	Paladin	Mountain Bike	f	82603zeunj	9149189752
7707568500	Virtue	Hybird Bike	f	57875ybgyd	3998219323
5030450187	Adonis	Comfort Bike	f	35123bcoml	5148092163
5913641843	All Star	Triathlon Bike	f	95847vroeq	5214768741
6175065676	Valiant	Time Trial Bike	t	14283rjsbe	5996959807
5491339677	Champion	BMX Bike	f	83631zntvy	3075377380
5883229899	Blink	Trick Bike	t	07717dktkx	4469252291
6830851012	Paragon	Commuting Bike	f	74776kgrfu	4626148636
6148386169	Iron	Cyclocross Bike	t	78444cyymk	9363730799
4700719454	Powerlift	Fixed Gear	f	02234frlyc	6645766128
1914491831	Maximus	Track Bike	t	72979dgmko	2890432873
6890797479	Paladin	Folding Bike	f	80518xpqcc	2064546969
7020393352	Adonis	BMX Bike	f	76166fzebv	8025852779
5239918453	All Star	Beach Cruiser	t	96802mffoa	4647444213
5592820241	Valiant	Comfort Bike	t	71095etfav	8918435344
1376300062	Champion	Hybird Bike	f	11167kfqsv	5105047188
6759458308	Powerlift	Mountain Bike	f	10260xtlct	6742917276
7210486846	Heroine	Triathlon Bike	t	55763zfkpb	2935403024
7675313474	Agile	Commuting Bike	t	38835ibdzg	8368924743
6849050033	Turbo	Fixed Gear	t	23284fvmdm	3219251815
9151849413	Apollo	Mountain Bike	t	82603zeunj	7268249915
2764292488	Armor	Folding Bike	t	57875ybgyd	7687053121
6052896889	Knight	Time Trial Bike	t	35123bcoml	8777843452
7799584746	Blink	Fixed Gear	t	95847vroeq	8030644827
1278212286	Virtue	Mountain Bike	f	14283rjsbe	1691875253
3671879154	Balanced	Cyclocross Bike	t	83631zntvy	3240423501
6507178510	Swift	Folding Bike	f	07717dktkx	3798361905
6073009026	Maximus	Triathlon Bike	f	74776kgrfu	3306649912
6977220455	Achilles	Mountain Bike	f	78444cyymk	8693543197
3373518528	Olympia	Fixed Gear	f	02234frlyc	9657487871
9274471006	Barbell	Comfort Bike	f	72979dgmko	6886200720
4325557029	Knight	Hybird Bike	t	80518xpqcc	2478836858
3157493341	Boulder	Commuting Bike	t	96802mffoa	7017915128
1924667002	Legion	Track Bike	t	71095etfav	1106065915
4955449246	Artemis	Comfort Bike	t	11167kfqsv	1962223574
8143518282	Vanguard	Cyclocross Bike	f	10260xtlct	1424222828
4706723123	Affinity	Time Trial Bike	t	55763zfkpb	8281116731
8588954889	Conquer	BMX Bike	t	38835ibdzg	3279081878
8100609914	Samurai	Trick Bike	t	23284fvmdm	5272592791
9150727205	All Star	Commuting Bike	f	82603zeunj	5610582560
9168412789	Turbo	Track Bike	f	57875ybgyd	3750241908
6500215158	Paragon	Triathlon Bike	f	35123bcoml	3623014849
3927503961	Iron	Comfort Bike	t	95847vroeq	7761353247
4608545041	Elite	Triathlon Bike	f	14283rjsbe	9166517431
9999482046	Crafty	BMX Bike	f	83631zntvy	7182858885
4305358562	Exert	Cyclocross Bike	f	07717dktkx	4347265556
1859224963	Energise	Hybird Bike	f	74776kgrfu	1466981905
6502094853	Paladin	Trick Bike	f	78444cyymk	4371123076
7962884733	Balanced	Comfort Bike	f	02234frlyc	8668430755
2998245982	Apollo	Trick Bike	f	76166fzebv	5181172361
\.


--
-- Data for Name: stasiun; Type: TABLE DATA; Schema: bike_sharing; Owner: db2018002
--

COPY bike_sharing.stasiun (id_stasiun, alamat, lat, long, nama) FROM stdin;
82603zeunj	63714 Blackbird Court	28	112	Pristine Gardens Route
57875ybgyd	0 Crescent Oaks Junction	410	78	Hollow Rock Loop Line
35123bcoml	8 Vera Point	438	875	Serenity Hills Tracks
95847vroeq	6335 Ohio Avenue	47	12	New Haven Line
14283rjsbe	533 Delaware Lane	9	800	Serpent Channel Line
83631zntvy	569 Jana Drive	98	397	Redwood Route
07717dktkx	65 Park Meadow Pass	198	716	Shadow Lake Speed Line
74776kgrfu	45563 Sachs Avenue	236	465	Greenbelt Line
78444cyymk	06 Nelson Center	483	402	High Valley Route
02234frlyc	2 Oneill Parkway	9	34	Paradise Coast Tracks
72979dgmko	0 Cascade Plaza	53	700	Rivermouth Tracks
80518xpqcc	546 Atwood Plaza	499	1	Orchard Park Tracks
76166fzebv	95824 Holmberg Center	411	82	Snowman Line
96802mffoa	826 Hazelcrest Street	526	18	Sunset Tracks
71095etfav	7088 Ilene Pass	41	83	Western Line
11167kfqsv	822 Eagle Crest Hill	27	107	Wild Rose Line
10260xtlct	6 Anhalt Pass	15	1	Somerset Line
55763zfkpb	18 Michigan Crossing	539	53	Eastern Loop Line
38835ibdzg	88 Carioca Street	33	60	Midland Tracks
23284fvmdm	8 Portage Avenue	56	262	Boulder Shore Line
98303tmtil	41535 Quincy Crossing	41	-9	Schultz and Sons
97887cpuuf	4 Twin Pines Way	39	-9	Hane, Schultz and Watsica
34074qkdqk	31 Westport Lane	-52	-69	VonRueden-Dickinson
69374txmjx	87 Jana Alley	8	-71	Schultz-Kuhic
13995olzwn	4 South Terrace	48	-2	Walsh, Keeling and Osinski
81953mmaji	7818 Fordem Junction	12	10	Senger, Armstrong and Effertz
05876tvgpx	9914 Stone Corner Trail	24	116	Volkman and Sons
34856egpyx	523 Veith Court	54	18	Schneider-Kulas
39792xcnid	152 Mcguire Avenue	-26	28	Brekke, Gerlach and Crona
09761mfyny	45859 Golf View Place	50	23	Koepp, Turcotte and Zboncak
92870xdrju	340 Grover Alley	-8	113	Upton LLC
82182rkhkn	6 Carpenter Hill	41	22	Hoeger, Jaskolski and Morissette
95767qrzah	1202 Magdeline Point	55	23	Bernhard Inc
59817qgxhx	517 Portage Alley	42	46	Huels-Ryan
93812jwroq	57030 Porter Circle	54	10	Jaskolski, Beier and Heller
03773emjre	6 Chive Alley	53	13	D'Amore, West and Walter
44977ncosu	1829 Troy Point	23	120	Murphy-Daugherty
83095uslnh	12964 Butterfield Avenue	-8	113	Kreiger-Koepp
62030ymtoa	66 Eliot Point	30	116	Hilll-Terry
45164kiyjh	33 Karstens Road	56	37	Larson-McCullough
74851wnism	2 Independence Park	13	101	Boehm Group
40451bfxpc	5 Garrison Circle	53	-1	Howell and Sons
81307jltbl	327 Schurz Park	-18	-50	Schmidt, Spinka and Treutel
79919hzkqq	27280 Northfield Drive	40	117	Huels Inc
95071oyzes	4835 Barby Court	30	105	Mayer, Schimmel and Cummings
94375cephj	165 Nobel Street	25	104	Crona LLC
62290vuypd	60 Lien Circle	25	107	Willms-Lindgren
91564cgsvo	6 4th Circle	11	-72	Nicolas, Graham and Ullrich
81229qqwea	437 American Ash Pass	47	88	O'Hara, Kohler and Rodriguez
20501dnyrk	1726 Calypso Circle	-34	151	Boehm-Kozey
\.


--
-- Data for Name: transaksi; Type: TABLE DATA; Schema: bike_sharing; Owner: db2018002
--

COPY bike_sharing.transaksi (no_kartu_anggota, date_time, jenis, nominal) FROM stdin;
9149189752	2019-12-01 08:00:00	PEMINJAMAN	4000
3998219323	2019-12-01 09:00:00	PEMINJAMAN	6000
5148092163	2019-12-01 10:00:00	PEMINJAMAN	2000
5214768741	2019-12-01 11:00:00	PEMINJAMAN	2000
5996959807	2019-12-01 12:00:00	PEMINJAMAN	2000
3075377380	2019-12-01 13:00:00	PEMINJAMAN	8000
4469252291	2019-12-01 14:00:00	PEMINJAMAN	4000
4626148636	2019-12-01 15:00:00	PEMINJAMAN	2000
9363730799	2019-12-02 08:00:00	PEMINJAMAN	9000
6645766128	2019-12-02 08:30:00	DENDA	50000
2890432873	2019-12-02 09:00:00	DENDA	50000
2064546969	2019-12-02 09:30:00	DENDA	50000
8025852779	2019-12-02 10:00:00	PEMINJAMAN	4000
4647444213	2019-12-02 10:30:00	DENDA	50000
8918435344	2019-12-02 11:00:00	PEMINJAMAN	7000
5105047188	2019-12-02 11:30:00	DENDA	50000
6742917276	2019-12-02 12:00:00	PEMINJAMAN	7000
2935403024	2019-12-02 12:30:00	DENDA	50000
8368924743	2019-12-02 13:00:00	DENDA	50000
3219251815	2019-12-02 13:30:00	PEMINJAMAN	3000
7268249915	2019-12-02 14:00:00	PEMINJAMAN	3000
7687053121	2019-12-02 14:30:00	PEMINJAMAN	4000
8777843452	2019-12-02 15:00:00	DENDA	50000
8030644827	2019-12-02 15:30:00	PEMINJAMAN	4000
1691875253	2019-12-02 16:00:00	DENDA	3000000
3240423501	2019-12-03 08:00:00	DENDA	50000
3798361905	2019-12-04 08:30:00	DENDA	50000
3306649912	2019-12-05 09:00:00	DENDA	50000
8693543197	2019-12-06 09:30:00	PEMINJAMAN	2000
9657487871	2019-12-07 10:00:00	DENDA	50000
6886200720	2019-12-08 10:30:00	PEMINJAMAN	2000
2478836858	2019-12-09 11:00:00	DENDA	50000
5181172361	2019-12-10 11:30:00	DENDA	50000
7017915128	2019-12-11 12:00:00	DENDA	50000
1106065915	2019-12-12 12:30:00	PEMINJAMAN	3000
1962223574	2019-12-13 13:00:00	DENDA	50000
1424222828	2019-12-14 13:30:00	DENDA	50000
8281116731	2019-12-15 14:00:00	DENDA	50000
3279081878	2019-12-16 08:00:00	PEMINJAMAN	4000
5272592791	2019-12-17 08:00:00	PEMINJAMAN	4000
5610582560	2019-12-18 08:00:00	PEMINJAMAN	3000
3750241908	2019-12-19 08:00:00	DENDA	50000
3623014849	2019-12-20 08:00:00	DENDA	50000
7761353247	2019-12-21 08:00:00	DENDA	3000000
9166517431	2019-12-22 08:00:00	DENDA	50000
7182858885	2019-12-23 08:00:00	DENDA	50000
4347265556	2019-12-24 08:00:00	PEMINJAMAN	5000
1466981905	2019-12-25 08:00:00	PEMINJAMAN	6000
4253457701	2019-12-25 08:30:00	DENDA	50000
8668430755	2019-12-25 09:00:00	DENDA	50000
1910748638	2019-12-25 09:30:00	PEMINJAMAN	2000
2389575077	2019-12-25 10:00:00	DENDA	50000
6195620957	2019-12-25 11:00:00	DENDA	50000
7129431658	2019-12-25 12:00:00	PEMINJAMAN	2000
3672180261	2019-12-25 12:30:00	DENDA	50000
8719052629	2019-12-25 13:00:00	DENDA	50000
8959826142	2019-12-25 13:30:00	PEMINJAMAN	4000
6991968211	2019-12-25 14:00:00	DENDA	50000
5105856747	2019-12-25 14:30:00	DENDA	3000000
9435880197	2019-12-25 15:00:00	DENDA	50000
7564879146	2019-12-25 15:30:00	DENDA	50000
9522484954	2019-12-25 16:00:00	PEMINJAMAN	4000
1534736738	2019-12-25 16:30:00	PEMINJAMAN	3000
6109449585	2019-12-25 17:00:00	PEMINJAMAN	5000
1459534240	2019-12-26 08:00:00	PEMINJAMAN	5000
5314254023	2019-12-26 08:30:00	DENDA	50000
7605910199	2019-12-26 09:00:00	PEMINJAMAN	6000
2270200232	2019-12-26 09:30:00	PEMINJAMAN	5000
2798530287	2019-12-26 10:00:00	PEMINJAMAN	4000
1187226485	2019-12-26 10:30:00	PEMINJAMAN	6000
5516271674	2019-12-26 11:00:00	DENDA	50000
7361041422	2019-12-26 11:30:00	DENDA	50000
5859087413	2019-12-26 12:00:00	DENDA	50000
9681539083	2019-12-26 12:30:00	DENDA	50000
2557960076	2019-12-26 13:00:00	DENDA	50000
8379265511	2019-12-26 13:30:00	PEMINJAMAN	6000
7971565005	2019-12-26 14:00:00	PEMINJAMAN	6000
9975399997	2019-12-26 14:30:00	DENDA	50000
2963452256	2019-12-26 15:00:00	DENDA	50000
3640102210	2019-12-26 15:30:00	PEMINJAMAN	9000
3495158744	2019-12-26 16:00:00	PEMINJAMAN	3000
4562780828	2019-12-26 16:30:00	DENDA	3000000
5064108343	2019-12-26 17:00:00	PEMINJAMAN	5000
1711407956	2019-12-27 08:00:00	DENDA	50000
2247662037	2019-12-28 08:00:00	PEMINJAMAN	4000
9681571865	2019-12-29 08:00:00	PEMINJAMAN	4000
2060971946	2019-12-30 08:00:00	DENDA	50000
6226946821	2019-12-30 12:00:00	DENDA	50000
2587202383	2019-12-30 15:00:00	DENDA	50000
1128961932	2019-12-31 08:00:00	DENDA	50000
8047287471	2019-12-31 09:00:00	DENDA	50000
3775569364	2019-12-31 10:00:00	PEMINJAMAN	5000
8928320972	2019-12-31 11:00:00	DENDA	50000
3221489332	2019-12-31 12:00:00	PEMINJAMAN	4000
1932696029	2019-12-31 13:00:00	DENDA	50000
5042113319	2019-12-31 14:00:00	PEMINJAMAN	5000
5687039417	2019-12-31 15:00:00	PEMINJAMAN	3000
2445831414	2019-12-31 15:30:00	PEMINJAMAN	7000
1197062072	2019-12-31 16:00:00	PEMINJAMAN	5000
4371123076	2019-12-25 08:00:01	PEMINJAMAN	4000
\.


--
-- Data for Name: transaksi_khusus_peminjaman; Type: TABLE DATA; Schema: bike_sharing; Owner: db2018002
--

COPY bike_sharing.transaksi_khusus_peminjaman (no_kartu_anggota, date_time, datetime_pinjam, no_sepeda, id_stasiun, no_kartu_peminjam) FROM stdin;
9149189752	2019-12-01 08:00:00	2019-05-03 22:44:47	5888268302	82603zeunj	9149189752
3998219323	2019-12-01 09:00:00	2019-04-25 00:09:37	7707568500	57875ybgyd	3998219323
5148092163	2019-12-01 10:00:00	2019-04-25 22:31:39	5030450187	35123bcoml	5148092163
5214768741	2019-12-01 11:00:00	2019-04-25 18:32:16	5913641843	95847vroeq	5214768741
5996959807	2019-12-01 12:00:00	2019-04-09 22:26:01	6175065676	14283rjsbe	5996959807
3075377380	2019-12-01 13:00:00	2019-04-14 14:33:39	5491339677	83631zntvy	3075377380
4469252291	2019-12-01 14:00:00	2019-04-22 12:35:41	5883229899	07717dktkx	4469252291
4626148636	2019-12-01 15:00:00	2019-04-28 11:15:48	6830851012	74776kgrfu	4626148636
9363730799	2019-12-02 08:00:00	2019-04-24 06:35:56	6148386169	78444cyymk	9363730799
6645766128	2019-12-02 08:30:00	2019-05-01 13:27:30	4700719454	02234frlyc	6645766128
2890432873	2019-12-02 09:00:00	2019-05-05 11:47:10	1914491831	72979dgmko	2890432873
2064546969	2019-12-02 09:30:00	2019-04-16 21:22:30	6890797479	80518xpqcc	2064546969
8025852779	2019-12-02 10:00:00	2019-04-13 12:07:20	7020393352	76166fzebv	8025852779
4647444213	2019-12-02 10:30:00	2019-04-11 06:55:45	5239918453	96802mffoa	4647444213
8918435344	2019-12-02 11:00:00	2019-05-06 19:54:55	5592820241	71095etfav	8918435344
5105047188	2019-12-02 11:30:00	2019-05-02 09:41:19	1376300062	11167kfqsv	5105047188
6742917276	2019-12-02 12:00:00	2019-04-11 11:22:09	6759458308	10260xtlct	6742917276
2935403024	2019-12-02 12:30:00	2019-04-28 02:06:38	7210486846	55763zfkpb	2935403024
8368924743	2019-12-02 13:00:00	2019-04-13 09:36:05	7675313474	38835ibdzg	8368924743
3219251815	2019-12-02 13:30:00	2019-04-23 08:34:06	6849050033	23284fvmdm	3219251815
7268249915	2019-12-02 14:00:00	2019-05-07 13:40:51	9151849413	82603zeunj	7268249915
7687053121	2019-12-02 14:30:00	2019-05-08 22:16:18	2764292488	57875ybgyd	7687053121
8777843452	2019-12-02 15:00:00	2019-04-26 14:11:56	6052896889	35123bcoml	8777843452
8030644827	2019-12-02 15:30:00	2019-05-07 12:40:10	7799584746	95847vroeq	8030644827
1691875253	2019-12-02 16:00:00	2019-05-02 22:42:14	1278212286	14283rjsbe	1691875253
3240423501	2019-12-03 08:00:00	2019-04-09 22:06:27	3671879154	83631zntvy	3240423501
3798361905	2019-12-04 08:30:00	2019-04-21 12:30:37	6507178510	07717dktkx	3798361905
3306649912	2019-12-05 09:00:00	2019-04-29 03:54:16	6073009026	74776kgrfu	3306649912
8693543197	2019-12-06 09:30:00	2019-04-19 02:31:12	6977220455	78444cyymk	8693543197
9657487871	2019-12-07 10:00:00	2019-04-19 15:55:22	3373518528	02234frlyc	9657487871
6886200720	2019-12-08 10:30:00	2019-05-05 14:44:06	9274471006	72979dgmko	6886200720
2478836858	2019-12-09 11:00:00	2019-04-15 01:33:51	4325557029	80518xpqcc	2478836858
5181172361	2019-12-10 11:30:00	2019-05-08 20:07:11	2998245982	76166fzebv	5181172361
7017915128	2019-12-11 12:00:00	2019-04-25 20:16:36	3157493341	96802mffoa	7017915128
1106065915	2019-12-12 12:30:00	2019-05-07 04:24:18	1924667002	71095etfav	1106065915
1962223574	2019-12-13 13:00:00	2019-04-15 02:00:38	4955449246	11167kfqsv	1962223574
1424222828	2019-12-14 13:30:00	2019-04-12 03:24:14	8143518282	10260xtlct	1424222828
8281116731	2019-12-15 14:00:00	2019-04-16 00:32:22	4706723123	55763zfkpb	8281116731
3279081878	2019-12-16 08:00:00	2019-04-24 08:54:50	8588954889	38835ibdzg	3279081878
5272592791	2019-12-17 08:00:00	2019-05-07 18:56:34	8100609914	23284fvmdm	5272592791
\.


--
-- Data for Name: voucher; Type: TABLE DATA; Schema: bike_sharing; Owner: db2018002
--

COPY bike_sharing.voucher (id_voucher, nama, kategori, nilai_poin, deskripsi, no_kartu_anggota) FROM stdin;
R1X3RcNk16	VCHR-KEPFT-2019	Green	53	10% off	4371123076
E5Uj14L013	VCHR-HSIVI-2019	Red	31	15% off	4253457701
Do0j7p5q82	VCHR-OLRIZ-2019	Yellow	78	20% off	8668430755
FuLcP57r33	VCHR-PEPQJ-2019	Green	56	25% off	1910748638
JrYuMx6l94	VCHR-RVGVP-2019	Red	24	50% off	2389575077
SwTm91Nt25	VCHR-MPPFS-2019	Yellow	48	10% off	6195620957
O73h5k9f21	VCHR-GWDCP-2019	Green	3	15% off	7129431658
FsGw7lVx14	VCHR-MGEPD-2019	Red	63	20% off	3672180261
UpCbRyEe02	VCHR-XCRUY-2019	Yellow	56	25% off	8719052629
VyY2JvW214	VCHR-RXCWY-2019	Green	30	50% off	8959826142
LkF0VeVd31	VCHR-QEMFL-2019	Red	55	10% off	6991968211
KdSgQbN032	VCHR-SGBIJ-2019	Yellow	12	15% off	5105856747
GuUcCiLd83	VCHR-TKREV-2019	Green	28	20% off	9435880197
RvG38zA901	VCHR-TFIKD-2019	Red	35	25% off	7564879146
VcZ8ExX014	VCHR-KNBYV-2019	Yellow	64	50% off	9522484954
QoW20v6447	VCHR-RFDOV-2019	Green	45	10% off	1534736738
P29fSjRg43	VCHR-DFNNR-2019	Red	40	15% off	6109449585
MrQlW8A137	VCHR-NVNWA-2019	Yellow	99	20% off	1459534240
Z2AtYbYq90	VCHR-ECBVU-2019	Green	92	25% off	5314254023
IjGvWwFa46	VCHR-WTSCJ-2019	Red	38	50% off	7605910199
Uj2e0uBe37	VCHR-GXNAZ-2019	Yellow	13	10% off	2270200232
Ze0vCvXf79	VCHR-WTUAL-2019	Green	11	15% off	2798530287
LiIlK5Mr26	VCHR-ANFHE-2019	Red	95	20% off	1187226485
UtGxXmV923	VCHR-OTDSF-2019	Yellow	10	25% off	5516271674
G8Q5KfQ964	VCHR-XCDHF-2019	Green	29	50% off	7361041422
DbLiSoDx28	VCHR-FTNCY-2019	Red	25	10% off	5859087413
E1HyHhQz42	VCHR-XPRWK-2019	Yellow	46	15% off	9681539083
QvIeZpN694	VCHR-YRAGC-2019	Green	58	20% off	2557960076
K9RsGvZ818	VCHR-ZKHOZ-2019	Red	87	25% off	8379265511
QkFxDc9370	VCHR-LIQQC-2019	Yellow	55	50% off	7971565005
Xc606rGi44	VCHR-LUXMG-2019	Green	58	10% off	9975399997
HdIi7vZa11	VCHR-VXAAF-2019	Red	7	15% off	2963452256
X54fJl9c47	VCHR-TYKVM-2019	Yellow	16	20% off	3640102210
GzYnB4Sb38	VCHR-EYRZJ-2019	Green	11	25% off	3495158744
BuObSpL901	VCHR-KRBXG-2019	Red	98	50% off	4562780828
LuViBr2626	VCHR-TCKJN-2019	Yellow	57	10% off	5064108343
QgEt8uQg77	VCHR-CRWZT-2019	Green	7	15% off	1711407956
UxAyUbEf89	VCHR-IDDAX-2019	Red	69	20% off	2247662037
V8S6WiRp19	VCHR-HHRLR-2019	Yellow	53	25% off	9681571865
Qg9vT26r97	VCHR-ZHATO-2019	Green	75	50% off	2060971946
RpL4SmXf61	VCHR-HZALA-2019	Red	92	10% off	6226946821
Fb5qDtIe63	VCHR-GXWUF-2019	Yellow	37	15% off	2587202383
F82i4kY550	VCHR-TBLOY-2019	Green	73	20% off	1128961932
FePtKzZs42	VCHR-RFPNS-2019	Red	71	25% off	8047287471
AmR62p3j25	VCHR-TYOKI-2019	Yellow	71	50% off	3775569364
Pk2aZg3b24	VCHR-UIGWC-2019	Green	19	10% off	8928320972
RwYvR6Id20	VCHR-YCPUH-2019	Red	24	15% off	3221489332
VrC3RnNn50	VCHR-VRNAT-2019	Yellow	85	20% off	1932696029
Y6RhQsV313	VCHR-KSXBT-2019	Green	57	25% off	5042113319
Zv9uGe9d93	VCHR-JUBSY-2019	Red	51	50% off	5687039417
\.


--
-- Name: acara acara_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.acara
    ADD CONSTRAINT acara_pkey PRIMARY KEY (id_acara);


--
-- Name: acara_stasiun acara_stasiun_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.acara_stasiun
    ADD CONSTRAINT acara_stasiun_pkey PRIMARY KEY (id_stasiun, id_acara);


--
-- Name: anggota anggota_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.anggota
    ADD CONSTRAINT anggota_pkey PRIMARY KEY (no_kartu);


--
-- Name: laporan laporan_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.laporan
    ADD CONSTRAINT laporan_pkey PRIMARY KEY (id_laporan, no_kartu_anggota, datetime_pinjam, nomor_sepeda, id_stasiun);


--
-- Name: peminjaman peminjaman_datetime_pinjam_key; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.peminjaman
    ADD CONSTRAINT peminjaman_datetime_pinjam_key UNIQUE (datetime_pinjam);


--
-- Name: peminjaman peminjaman_id_stasiun_key; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.peminjaman
    ADD CONSTRAINT peminjaman_id_stasiun_key UNIQUE (id_stasiun);


--
-- Name: peminjaman peminjaman_no_kartu_anggota_key; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.peminjaman
    ADD CONSTRAINT peminjaman_no_kartu_anggota_key UNIQUE (no_kartu_anggota);


--
-- Name: peminjaman peminjaman_nomor_sepeda_key; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.peminjaman
    ADD CONSTRAINT peminjaman_nomor_sepeda_key UNIQUE (nomor_sepeda);


--
-- Name: peminjaman peminjaman_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.peminjaman
    ADD CONSTRAINT peminjaman_pkey PRIMARY KEY (no_kartu_anggota, datetime_pinjam, nomor_sepeda, id_stasiun);


--
-- Name: penugasan penugasan_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.penugasan
    ADD CONSTRAINT penugasan_pkey PRIMARY KEY (ktp, start_datetime, id_stasiun);


--
-- Name: person person_email_key; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.person
    ADD CONSTRAINT person_email_key UNIQUE (email);


--
-- Name: person person_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.person
    ADD CONSTRAINT person_pkey PRIMARY KEY (ktp);


--
-- Name: petugas petugas_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.petugas
    ADD CONSTRAINT petugas_pkey PRIMARY KEY (ktp);


--
-- Name: sepeda sepeda_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.sepeda
    ADD CONSTRAINT sepeda_pkey PRIMARY KEY (nomor);


--
-- Name: stasiun stasiun_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.stasiun
    ADD CONSTRAINT stasiun_pkey PRIMARY KEY (id_stasiun);


--
-- Name: transaksi transaksi_date_time_key; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.transaksi
    ADD CONSTRAINT transaksi_date_time_key UNIQUE (date_time);


--
-- Name: transaksi_khusus_peminjaman transaksi_khusus_peminjaman_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.transaksi_khusus_peminjaman
    ADD CONSTRAINT transaksi_khusus_peminjaman_pkey PRIMARY KEY (no_kartu_anggota, date_time);


--
-- Name: transaksi transaksi_no_kartu_anggota_key; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.transaksi
    ADD CONSTRAINT transaksi_no_kartu_anggota_key UNIQUE (no_kartu_anggota);


--
-- Name: transaksi transaksi_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.transaksi
    ADD CONSTRAINT transaksi_pkey PRIMARY KEY (no_kartu_anggota, date_time);


--
-- Name: voucher voucher_pkey; Type: CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.voucher
    ADD CONSTRAINT voucher_pkey PRIMARY KEY (id_voucher);


--
-- Name: peminjaman cek_peminjaman; Type: TRIGGER; Schema: bike_sharing; Owner: db2018002
--

CREATE TRIGGER cek_peminjaman AFTER INSERT OR UPDATE ON bike_sharing.peminjaman FOR EACH ROW EXECUTE PROCEDURE bike_sharing.cek_peminjaman_lebih_satu_hari();


--
-- Name: transaksi_khusus_peminjaman cek_pengembalian_sepeda; Type: TRIGGER; Schema: bike_sharing; Owner: db2018002
--

CREATE TRIGGER cek_pengembalian_sepeda BEFORE INSERT ON bike_sharing.transaksi_khusus_peminjaman FOR EACH ROW EXECUTE PROCEDURE bike_sharing.pengembalian_sepeda();


--
-- Name: laporan check_laporan_peminjaman_24jam; Type: TRIGGER; Schema: bike_sharing; Owner: db2018002
--

CREATE TRIGGER check_laporan_peminjaman_24jam BEFORE INSERT ON bike_sharing.laporan FOR EACH ROW EXECUTE PROCEDURE bike_sharing.laporan_peminjaman_24jam();


--
-- Name: peminjaman check_update_denda_10jam; Type: TRIGGER; Schema: bike_sharing; Owner: db2018002
--

CREATE TRIGGER check_update_denda_10jam BEFORE INSERT ON bike_sharing.peminjaman FOR EACH ROW EXECUTE PROCEDURE bike_sharing.update_denda_10jam();


--
-- Name: voucher trigger_klaim_voucher; Type: TRIGGER; Schema: bike_sharing; Owner: db2018002
--

CREATE TRIGGER trigger_klaim_voucher AFTER UPDATE OF no_kartu_anggota ON bike_sharing.voucher FOR EACH ROW EXECUTE PROCEDURE bike_sharing.klaim_voucher();


--
-- Name: acara_stasiun acara_stasiun_id_acara_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.acara_stasiun
    ADD CONSTRAINT acara_stasiun_id_acara_fkey FOREIGN KEY (id_acara) REFERENCES bike_sharing.acara(id_acara) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: acara_stasiun acara_stasiun_id_stasiun_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.acara_stasiun
    ADD CONSTRAINT acara_stasiun_id_stasiun_fkey FOREIGN KEY (id_stasiun) REFERENCES bike_sharing.stasiun(id_stasiun) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: anggota anggota_ktp_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.anggota
    ADD CONSTRAINT anggota_ktp_fkey FOREIGN KEY (ktp) REFERENCES bike_sharing.person(ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: laporan laporan_datetime_pinjam_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.laporan
    ADD CONSTRAINT laporan_datetime_pinjam_fkey FOREIGN KEY (datetime_pinjam) REFERENCES bike_sharing.peminjaman(datetime_pinjam) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: laporan laporan_id_stasiun_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.laporan
    ADD CONSTRAINT laporan_id_stasiun_fkey FOREIGN KEY (id_stasiun) REFERENCES bike_sharing.peminjaman(id_stasiun) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: laporan laporan_no_kartu_anggota_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.laporan
    ADD CONSTRAINT laporan_no_kartu_anggota_fkey FOREIGN KEY (no_kartu_anggota) REFERENCES bike_sharing.peminjaman(no_kartu_anggota) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: laporan laporan_nomor_sepeda_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.laporan
    ADD CONSTRAINT laporan_nomor_sepeda_fkey FOREIGN KEY (nomor_sepeda) REFERENCES bike_sharing.peminjaman(nomor_sepeda) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: peminjaman peminjaman_id_stasiun_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.peminjaman
    ADD CONSTRAINT peminjaman_id_stasiun_fkey FOREIGN KEY (id_stasiun) REFERENCES bike_sharing.stasiun(id_stasiun) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: peminjaman peminjaman_no_kartu_anggota_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.peminjaman
    ADD CONSTRAINT peminjaman_no_kartu_anggota_fkey FOREIGN KEY (no_kartu_anggota) REFERENCES bike_sharing.anggota(no_kartu) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: peminjaman peminjaman_nomor_sepeda_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.peminjaman
    ADD CONSTRAINT peminjaman_nomor_sepeda_fkey FOREIGN KEY (nomor_sepeda) REFERENCES bike_sharing.sepeda(nomor) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: penugasan penugasan_id_stasiun_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.penugasan
    ADD CONSTRAINT penugasan_id_stasiun_fkey FOREIGN KEY (id_stasiun) REFERENCES bike_sharing.stasiun(id_stasiun) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: penugasan penugasan_ktp_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.penugasan
    ADD CONSTRAINT penugasan_ktp_fkey FOREIGN KEY (ktp) REFERENCES bike_sharing.petugas(ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: petugas petugas_ktp_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.petugas
    ADD CONSTRAINT petugas_ktp_fkey FOREIGN KEY (ktp) REFERENCES bike_sharing.person(ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: sepeda sepeda_id_stasiun_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.sepeda
    ADD CONSTRAINT sepeda_id_stasiun_fkey FOREIGN KEY (id_stasiun) REFERENCES bike_sharing.stasiun(id_stasiun) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: sepeda sepeda_no_kartu_penyumbang_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.sepeda
    ADD CONSTRAINT sepeda_no_kartu_penyumbang_fkey FOREIGN KEY (no_kartu_penyumbang) REFERENCES bike_sharing.anggota(no_kartu) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_khusus_peminjaman transaksi_khusus_peminjaman_date_time_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.transaksi_khusus_peminjaman
    ADD CONSTRAINT transaksi_khusus_peminjaman_date_time_fkey FOREIGN KEY (date_time) REFERENCES bike_sharing.transaksi(date_time) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_khusus_peminjaman transaksi_khusus_peminjaman_datetime_pinjam_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.transaksi_khusus_peminjaman
    ADD CONSTRAINT transaksi_khusus_peminjaman_datetime_pinjam_fkey FOREIGN KEY (datetime_pinjam) REFERENCES bike_sharing.peminjaman(datetime_pinjam) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_khusus_peminjaman transaksi_khusus_peminjaman_id_stasiun_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.transaksi_khusus_peminjaman
    ADD CONSTRAINT transaksi_khusus_peminjaman_id_stasiun_fkey FOREIGN KEY (id_stasiun) REFERENCES bike_sharing.peminjaman(id_stasiun) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_khusus_peminjaman transaksi_khusus_peminjaman_no_kartu_anggota_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.transaksi_khusus_peminjaman
    ADD CONSTRAINT transaksi_khusus_peminjaman_no_kartu_anggota_fkey FOREIGN KEY (no_kartu_anggota) REFERENCES bike_sharing.transaksi(no_kartu_anggota) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_khusus_peminjaman transaksi_khusus_peminjaman_no_kartu_peminjam_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.transaksi_khusus_peminjaman
    ADD CONSTRAINT transaksi_khusus_peminjaman_no_kartu_peminjam_fkey FOREIGN KEY (no_kartu_peminjam) REFERENCES bike_sharing.peminjaman(no_kartu_anggota) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_khusus_peminjaman transaksi_khusus_peminjaman_no_sepeda_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.transaksi_khusus_peminjaman
    ADD CONSTRAINT transaksi_khusus_peminjaman_no_sepeda_fkey FOREIGN KEY (no_sepeda) REFERENCES bike_sharing.peminjaman(nomor_sepeda) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi transaksi_no_kartu_anggota_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.transaksi
    ADD CONSTRAINT transaksi_no_kartu_anggota_fkey FOREIGN KEY (no_kartu_anggota) REFERENCES bike_sharing.anggota(no_kartu) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: voucher voucher_no_kartu_anggota_fkey; Type: FK CONSTRAINT; Schema: bike_sharing; Owner: db2018002
--

ALTER TABLE ONLY bike_sharing.voucher
    ADD CONSTRAINT voucher_no_kartu_anggota_fkey FOREIGN KEY (no_kartu_anggota) REFERENCES bike_sharing.anggota(no_kartu) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--
