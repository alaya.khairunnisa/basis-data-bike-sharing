from django.urls import path
from . import views

urlpatterns=[
    path(r'daftar_acara/',views.daftar_acara,name="daftar_acara"),
    path(r'daftar_acara/form',views.form_acara,name="form_acara"),
    path(r'daftar_acara/update',views.form_acara,name="update_acara"),
]


