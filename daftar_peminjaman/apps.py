from django.apps import AppConfig


class DaftarPeminjamanConfig(AppConfig):
    name = 'daftar_peminjaman'
